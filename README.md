<div align="center">
<img src="header.png" alt="Camera Tweaks" width="500"/><br>
https://steamcommunity.com/sharedfiles/filedetails/?id=2325017792<br>
A mod for [Besiege](https://store.steampowered.com/app/346010), a physics based building sandbox game. <br><br>

![Steam Views](https://img.shields.io/steam/views/2325017792?color=%231b2838&logo=steam&style=for-the-badge)
![Steam Subscriptions](https://img.shields.io/steam/subscriptions/2325017792?color=%231b2838&logo=steam&style=for-the-badge)
![Steam Favorites](https://img.shields.io/steam/favorites/2325017792?color=%231b2838&logo=steam&style=for-the-badge)
</div>

Camera Tweaks is a mod for [Besiege](https://store.steampowered.com/app/346010) which adds a few helpful camera features.

## Features
- Headlight (Ctrl + L)
- Roll Camera (Ctrl + Shift + Mouse Wheel)
- FoV Zoom Camera (Ctrl + Alt + Mouse Wheel)
- Increases the default view distance
- Dynamic shadow cascades (no more crappy shadows, no matter what range you're using)
- Copies FoV, orthographic settings, and render distances to all subordinate cameras (e.g. the 3D HUD camera).

## Technologies
* Unity 5.4 - the game engine Besiege runs on
* C# 3.0 - the scripting language that Unity 5.4 shipped with

## Installation
The latest version of the mod can be found on the Steam Workshop, and can be installed at the press of the big green 'Subscribe' button. It will automatically activate the next time you boot the game.

If, for whatever reason, you don't want to use Steam to manage your mods, installation is as follows:
1. Install Besiege.
2. Clone the repository into `Besiege/Besiege_Data/Mods`.
3. Open the `.sln` file in `src` in Visual Studio 2015 (or later). 
    - A bug means that VS versions *after* 2019 can only be used if VS2019 or earlier is installed with the `.NET Framework 3.5 development tools` component.
4. Press F6 to compile the mod.

## Status
This mod is feature-complete. <br>
However, additional features and fixes may be added in the future as necessary.

## Images
<img src="https://thumbs.gfycat.com/DevotedSentimentalChimneyswift-size_restricted.gif" alt="camera tracking at high speeds" width="500"/>
<img src="https://thumbs.gfycat.com/PointlessBruisedBlesbok-size_restricted.gif" alt="maintain target position when block is deleted" width="500"/>
<img src="https://i.imgur.com/mbTKY4p.png" alt="headlight" width="500"/>
