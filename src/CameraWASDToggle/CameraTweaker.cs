﻿using Modding;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace CameraControlOverhaul {
    internal class CameraTweaker : MonoBehaviour {


        ModKey rollKey; // ctrl + shift + wheel
        float roll = 0f;
        float rollSmoothed;
        bool isRolling = false;

        ModKey zoomKey; // ctrl + alt + wheel
        float fov;
        float fovSmoothed;
        bool isZooming = false;

        ModKey lightKey;
        Light headlight;

        List<Camera> cameras;

        public bool IsActive => MouseOrbit.Instance.isActive && !MouseOrbit.Instance.cinematic && !StatMaster.isHeadless && !(FixedCameraController.Instance?.activeCamera);
        float orbitSmooth => OptionsMaster.BesiegeConfig.SmoothCamera ? MouseOrbit.Instance.filmCamSmooth : MouseOrbit.Instance.smooth;
        float zoomSmooth => OptionsMaster.BesiegeConfig.SmoothCamera ? MouseOrbit.Instance.filmCamSmooth * 3 : MouseOrbit.Instance.zoomSmooth;

        void Start() {
            rollKey = ModKeys.GetKey("Roll Camera (+ scroll)");
            zoomKey = ModKeys.GetKey("Zoom Camera (+ scroll)");

            lightKey = ModKeys.GetKey("Toggle Headlight");

            cameras = GetComponentsInChildren<Camera>().ToList();

            headlight = gameObject.AddComponent<Light>();
            headlight.type = LightType.Spot;
            headlight.range = 250;
            headlight.spotAngle = 120;
            headlight.enabled = false;

            MouseOrbit.Instance.minZoom = 0.5f;
            MouseOrbit.maxZoom = 1250f;

            ReferenceMaster.onFOVChanged += OnFOVChanged;
            ReferenceMaster.onLocalMachineSimulation += OnMachineSimulationPre;

            Reset();
        }

        void OnMachineSimulationPre(bool sim) {
            if (!sim && FixedCameraController.Instance.activeCamera) {
                OnFOVChanged();
            }
        }

        void OnDestroy() {
            ReferenceMaster.onFOVChanged -= OnFOVChanged;
            ReferenceMaster.onLocalMachineSimulation -= OnMachineSimulationPre;
        }

        void Update() {
            if (!IsActive) {
                return;
            }
            if (!StatMaster.inMenu && !StatMaster.stopCamZoom) {
                if (!isRolling && !isZooming) { // mutually exclusive
                    if (rollKey.IsDown) { // for the record, i hate all these nested conditions
                        isRolling = true;
                        StatMaster.DisableCameraZoom(true);
                    }
                    else if (zoomKey.IsDown) {
                        isZooming = true;
                        StatMaster.DisableCameraZoom(true);
                    }
                }

                if (isRolling) {
                    roll += InputManager.ZoomValue() * 50;
                    roll = Mathf.Clamp(roll, -90, 90);
                }

                if (isZooming) {
                    fov -= InputManager.ZoomValue() * fov;
                    fov = Mathf.Clamp(fov, 5, OptionsMaster.GetVerticalFOV());
                }
            }
            if (!StatMaster.stopHotkeys && lightKey.IsPressed) {
                headlight.enabled = !headlight.enabled;
                /*if (LightButton) {
                    LightButton.isOn = Headlight.enabled;
                }*/
            }

            // smoothing
            rollSmoothed = Mathf.Lerp(rollSmoothed, Mathf.Round(roll / 5f) * 5, Time.unscaledDeltaTime * orbitSmooth);
            fovSmoothed = Mathf.Lerp(fovSmoothed, fov, Time.unscaledDeltaTime * zoomSmooth);

            // handle modifier key release
            if (isRolling && rollKey.IsReleased) {
                isRolling = false;
                StatMaster.DisableCameraZoom(false);
            }
            else if (isZooming && zoomKey.IsReleased) {
                isZooming = false;
                StatMaster.DisableCameraZoom(false);
            }

            if (InputManager.ResetCameraButton()) {
                Reset();
            }
        }

        // executes after MouseOrbit.LateUpdate thanks to load order
        void LateUpdate() {
            if (!IsActive) {
                return;
            }

            if (!InputManager.RotateCameraKeyHeld()) {
                this.transform.Rotate(Vector3.forward, rollSmoothed);
            }

            foreach (var cam in cameras) {
                if (!cam) {
                    continue;
                }
                cam.fieldOfView = fovSmoothed;
                cam.orthographic = MouseOrbit.Instance.cam.orthographic;
                cam.orthographicSize = MouseOrbit.Instance.cam.orthographicSize;
                cam.nearClipPlane = MouseOrbit.Instance.cam.nearClipPlane;
                cam.farClipPlane = MouseOrbit.Instance.cam.farClipPlane;
            }

            UpdateShadowCascades();
        }

        void OnFOVChanged() {
            fov = fovSmoothed = OptionsMaster.GetVerticalFOV();
        }

        void Reset() {
            roll = rollSmoothed = 0;
            fov = fovSmoothed = OptionsMaster.GetVerticalFOV();
        }

        void UpdateShadowCascades() {
            // 0, 25, 50, 100, 400
            // 0, 50, 250, 1000, 4000
            // 0, 0.625-0.0125, 0.125-0.0625, 0.25, 1

            float dist = QualitySettings.shadowDistance;
            QualitySettings.shadowCascade4Split = new Vector3(
                Mathf.Clamp(0.068f - 0.0000139f * dist, 0.0125f, 1f),
                Mathf.Clamp(0.132f - 0.0000174f * dist, 0.0625f, 1),
                0.25f);
            QualitySettings.shadowCascade2Split = Mathf.Clamp(0.132f - 0.0000174f * dist, 0.0625f, 1);
        }
    }
}
