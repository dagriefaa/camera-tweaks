using Modding;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CameraControlOverhaul {

    public class Mod : ModEntryPoint {

        public const string MOD_NAME = "CameraControlOverhaul";

        public static GameObject ModControllerObject { get; private set; }
        public override void OnLoad() {
            ModControllerObject = GameObject.Find("ModControllerObject");
            if (!ModControllerObject) UnityEngine.Object.DontDestroyOnLoad(ModControllerObject = new GameObject("ModControllerObject"));


            Events.OnActiveSceneChanged += OnSceneChanged;
            if (StatMaster.isMP) {
                OnSceneChanged(SceneManager.GetActiveScene(), SceneManager.GetActiveScene());
            }

            // object explorer
            /*if (Mods.IsModLoaded(new System.Guid("3c1fa3de-ec74-44e4-807c-9eced79ddd3f"))) {
                ModControllerObject.AddComponent<Mapper>();
            }*/

        }

        private void OnSceneChanged(Scene scene1, Scene scene2) {
            if (SceneNotPlayable()) { return; }
            Camera.main.gameObject.AddComponent<CameraTweaker>();
        }

        public static bool SceneNotPlayable() {
            return !AdvancedBlockEditor.Instance;
        }

    }
}
