[img=https://i.imgur.com/vOUgXZm.png][/img]
                                                      [b][url=https://gitlab.com/dagriefaa/camera-tweaks]GitLab Repository[/url][/b]

Adds a few helpful features to the free camera.

[url=https://steamcommunity.com/sharedfiles/filedetails/?id=2913469777][img]https://i.imgur.com/XGCiJRy.png[/img][/url]

[h1]Features[/h1]
◼ Headlight (Ctrl + L)
◼ Roll Camera (Ctrl + Shift + Mouse Wheel)
◼ FoV Zoom Camera (Ctrl + Alt + Mouse Wheel)
◼ Increases the default view distance
◼ Dynamic shadow cascades (no more crappy shadows, no matter what range you're using)
◼ Copies FoV, orthographic settings, and render distances to all subordinate cameras (e.g. the 3D HUD camera).

This mod used to be Camera Control Overhaul. It did a lot more, but most of it was fixing bugs which are now fixed in the base game and what remained wasn't terribly helpful, so it was pared back.